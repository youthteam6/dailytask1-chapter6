/**
 * @file contains request handler of post resource
 * @author Fikri Rahmat Nurhidayat
 */
const { Post } = require("../../../models");
const PostService = require("../../../services/post");

module.exports = {
  list(req, res) {
    PostService.getAllPosts()
      .then((posts) => {
        res.status(200).json({
          status: "OK",
          message: "Berhasil menampilkan semua post",
          data: {
            posts,
          },
        });
      })
      .catch((e) => {
        res.status(400).json({
          status: "FAILED",
          message: "Gagal menampilkan post",
        });
      });
  },

  create(req, res) {
    const { title, body } = req.body;
    PostService.createPost(title, body)
      .then((post) => {
        res.status(201).json({
          status: "OK",
          message: "Post berhasil ditambahkan",
          data: post,
        });
      })
      .catch((e) => {
        res.status(201).json({
          status: "FAILED",
          message: "Post gagal ditambahkan",
          message: e.message,
        });
      });
  },

  update(req, res) {
    const post = req.post;

    PostService.updatePost(post, req.body)
    .then(() => {
      res.status(200).json({
        status: "OK",
        message: "Berhasil update post",
        data: post,
      });
    })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    const post = req.post;

    res.status(200).json({
      status: "OK",
      data: post,
    });
  },

  destroy(req, res) {
    PostService.destroyPost(req.post)
    .then(() => {
      res.status(204).end();
    })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  setPost(req, res, next) {
    PostService.choosePost(req.params.id)
      .then((post) => {
        if (!post) {
          res.status(404).json({
            status: "FAIL",
            message: "Post not found!",
          });

          return;
        }

        req.post = post;
        next();
      })
      .catch((err) => {
        res.status(404).json({
          status: "FAIL",
          message: "Post not found!",
        });
      });
  },
};
